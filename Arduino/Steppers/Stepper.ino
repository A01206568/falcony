/* Falcon Y
 *  First stepper code
 *  Juan Manuel Ledesma
 *  Jose Alberto Barrera Clemente
 *  Reviewed by: Juan Manuel Ledesma
 *  26-10-2019
 */

//Define the velocity of the steps the smaller the size the faster the movement
#define VEL 1700

//Define the ports used
int steps = 2;
int dir = 3;
int reset = 9;
//Define the number of steps (cycle iterations)
int pasos = 3500;

//Define the setup loop, just put all ports as outputs
void setup(){
    pinMode(steps, OUTPUT);
    pinMode(dir, OUTPUT);
    pinMode(reset, OUTPUT);
}

//Define the main loop 
void loop(){
    //To turn off the motor, the port of the chip turn off so that no commands will be read
    digitalWrite(reset, LOW);
    delay(100);
    //To start the motor and begin to read the commands
    digitalWrite(reset, HIGH);

    //For the motor to move forward
    digitalWrite(dir, HIGH);
    //This is how a stepper must be manipulated, by small increments using a for
    for(int i = 0; i < pasos; i++){
        //The HIGH to LOW change enables the motor to advance
        digitalWrite(steps, HIGH);
        digitalWrite(steps, LOW);
        //The movement velocity
        delayMicroseconds(VEL);
    }

    //Same as above, to prevent reading other innecessary values 
    digitalWrite(reset, LOW);
    delay(100);
    digitalWrite(reset, HIGH);

    //For the motor to move backwards
    digitalWrite(dir, LOW);
    for(int i = 0; i < pasos; i++){
        //The HIGH to LOW change enables the motor to advance
        digitalWrite(steps, LOW);
        digitalWrite(steps, HIGH);
        delayMicroseconds(VEL);
    }
}
