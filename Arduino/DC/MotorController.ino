/* Falcon Y
 *  Second weheels code
 *  Juan Manuel Ledesma
 *  Jose Alberto Barrera Clemente
 *  Reviewed by: Juan Manuel Ledesma
 *  26-10-2019
 *  rosrun teleop_twist_keyboard teleop_twist_keyboard.py
 *  Source: https://atadiat.com/en/e-rosserial-arduino-introduction/
 */

//This code was given to us by Arturo Escobed, we only did some minor changes

//Include required libraries
#include <ros.h>
#include <ArduinoHardware.h>
#include <geometry_msgs/Twist.h>

//Define the constants
#define radio_llantas 0.0325
#define separacion_llantas 0.295
#define factor_escalamiento 65.0/9.0

//Instantiate the node handler
ros::NodeHandle nh;

//Create the class
class Motor {
    double w = 0;
    double wheel_rad = 0, wheel_sep = 0;
    double angular_speed = 0, linear_speed = 0, scaling_factor = 1;
    int EN_PIN, IN_1, IN_2;
    char SIDE;
    
  public:
    Motor() {
      this->EN_PIN = 2;
      this->IN_1 = 1;
      this->IN_2 = 2;
      this->SIDE = 'L';
    }
    Motor(int EN_PIN, int IN_1, int IN_2, char SIDE, double wheel_rad, double wheel_sep, double scaling_factor) {
      this->EN_PIN = EN_PIN;
      this->IN_1 = IN_1;
      this->IN_2 = IN_2;
      this->wheel_rad = wheel_rad;
      this->wheel_sep = wheel_sep;
      this->scaling_factor = scaling_factor;
    }

    void setup() {
      pinMode(EN_PIN, OUTPUT);
      pinMode(IN_1, OUTPUT);
      pinMode(IN_2, OUTPUT);
      digitalWrite(EN_PIN, LOW);
      digitalWrite(IN_1, LOW);
      digitalWrite(IN_2, LOW);
    }

    void drive() {
      int PULSE = this->w * this->scaling_factor;
      if (SIDE == 'L') {
        PULSE = -PULSE;
      }
      if (PULSE > 0) {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, HIGH);
        digitalWrite(IN_2, LOW);
      } else if (PULSE < 0) {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, LOW);
        digitalWrite(IN_2, HIGH);
      } else {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, LOW);
        digitalWrite(IN_2, LOW);
      }
    }

    void drive(int PULSE) {
      if (SIDE == 'L') {
        PULSE = -PULSE;
      }
      if (PULSE > 0) {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, HIGH);
        digitalWrite(IN_2, LOW);
      } else if (PULSE < 0) {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, LOW);
        digitalWrite(IN_2, HIGH);
      } else {
        analogWrite(EN_PIN, abs(PULSE));
        digitalWrite(IN_1, LOW);
        digitalWrite(IN_2, LOW);
      }
    }

    void set_angular_frequency(double lin_speed, double ang_speed) {
      if (SIDE == 'L') {
        w = (lin_speed / wheel_rad) - ((ang_speed * wheel_sep) / (2.0 * wheel_rad));
      } else if (SIDE == 'R') {
        w = (lin_speed / wheel_rad) + ((ang_speed * wheel_sep) / (2.0 * wheel_rad));
      }
    }
};


Motor motorA_1, motorB_1, motorC_2, motorD_2, motorE_3, motorF_3;

void message_cb(const geometry_msgs::Twist& msg) {
  //cada que se recibe un mensaje que se hace
  motorA_1.set_angular_frequency(msg.linear.x, msg.angular.x);
  motorB_1.set_angular_frequency(msg.linear.x, msg.angular.x);
  motorC_2.set_angular_frequency(msg.linear.x, msg.angular.x);
  motorD_2.set_angular_frequency(msg.linear.x, msg.angular.x);
  motorE_3.set_angular_frequency(msg.linear.x, msg.angular.x);
  motorF_3.set_angular_frequency(msg.linear.x, msg.angular.x);
}
ros::Subscriber<geometry_msgs::Twist> sub("joy_twist/cmd_vel", &message_cb);

void Motors_init();
void MotorL(int PULSE);//dato a recibir falta
void MotorR(int PULSE);//dato a recibir falta

void setup()
{
  // put your setup code here, to run once:
  Motors_init();
  nh.initNode();
  nh.subscribe(sub);

}

void loop() {
  // put your main code here, to run repeatedly:
  motorA_1.drive();
  motorB_1.drive();
  motorC_2.drive();
  motorD_2.drive();
  motorE_3.drive();
  motorF_3.drive();

  nh.spinOnce();

}

void Motors_init() {
  motorA_1 = Motor(2, 22, 23, 'L', radio_llantas, separacion_llantas, factor_escalamiento);
  motorB_1 = Motor(3, 24, 25, 'R', radio_llantas, separacion_llantas, factor_escalamiento);
  motorC_2 = Motor(4, 26, 27, 'L', radio_llantas, separacion_llantas, factor_escalamiento);
  motorD_2 = Motor(5, 28, 29, 'R', radio_llantas, separacion_llantas, factor_escalamiento);
  motorE_3 = Motor(6, 30, 31, 'L', radio_llantas, separacion_llantas, factor_escalamiento);
  motorF_3 = Motor(7, 32, 33, 'R', radio_llantas, separacion_llantas, factor_escalamiento);
  motorA_1.setup();
  motorB_1.setup();
  motorC_2.setup();
  motorD_2.setup();
  motorE_3.setup();
  motorF_3.setup();
}
