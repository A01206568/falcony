#!/usr/bin/env python
#Declare which environment will run the following code

#  Falcon Y
#  camera_to_movement
#  Juan Manuel Ledesma
#  Jose Alberto Barrera Clemente
#  David Flores
#  Reviewed by: Juan Manuel Ledesma
#  26-10-2019

##Import the required libraries
#rospy for python-ros interaction
import rospy
#Twist for sending a message throught a topic
from geometry_msgs.msg import Twist

#Global variables for names and values
nombre_nodo="vision_motores"
nombre_topico_suscrito="center"
nombre_topico_publicar="cmd_vel"
radio_lim_sup=70
factor_angular=1
factor_linear=1
mitad_pantalla=300

#Callback for the subscriber
def callback(data):
    eje_x = data.linear.x
    radio = data.linear.z
    
    twist_motor = Twist()
    if radio < radio_lim_sup:
        #Publish here 1/twist.vision.linear.z
        twist_motor.linear.x = 1 * factor_linear
    else:
        twist_motor.linear.x = 0
        
    if eje_x > mitad_pantalla:
        twist_motor.angular.x = factor_angular * -1 # Movement to the right
    elif eje_x < mitad_pantalla:
        twist_motor.angular.x = factor_angular * 1 #Movement to the left
    else:
        twist_motor.angular.x = 0

    #Publish
    pub.publish(twist_motor)

#Define a start function
def start():
    #Global definition for a variable
    global pub
    #Instantiate the publisher object that will publish to topic cmd_vel a Twist message
    pub = rospy.Publisher(nombre_topico_publicar, Twist)
    #Instantiate a subscriber named center, that recieves Twist type messages and set the callback function
    rospy.Subscriber(nombre_topico_suscrito, Twist, callback)
    #Instantiate the node
    rospy.init_node(nombre_nodo)
    #Keep running the program until the node is shutdown
    rospy.spin()

#Main of the program
if __name__=="__main__":
    start()