#!/usr/bin/env python
#Declare which environment will run the following code

#  Falcon Y
#  joy_to_twist
#  Juan Manuel Ledesma
#  Jose Alberto Barrera Clemente
#  David Flores
#  Reviewed by: Juan Manuel Ledesma
#  26-10-2019

##Import the required libraries
#rospy for python-ros interaction
import rospy
#Twist for sending a message throught a topic
from geometry_msgs.msg import Twist 
#Joy for obtaining the joystick axes and buttons
from sensor_msgs.msg import Joy 

#Global variables for names
pub_name='joy_twist/cmd_vel'
sub_name="joy"
node_name='Joy2Twist'

#Callback for the subscriber
def callback(data):
    #Create an empty twist variable
	twist = Twist() 
    #Convert the information from the joystick (linear) and save it in the x component from the linear twist message
	twist.linear.x = data.axes[1] 
    #Convert the information from the joystick (angular) and save it in the x component from the angular twist message
	twist.angular.x = data.axes[3]
    #Publish the information to the topic
	pub.publish(twist)

#Define a start function
def start():
    #Global definition for a variable
	global pub
    #Instantiate the publisher object that will publish to topic joy_twist/cmd_vel a Twist message
	pub = rospy.Publisher(pub_name, Twist)
    #Instantiate a subscriber named joy, that recieves Joy type messages and set the callback function
	rospy.Subscriber(sub_name, Joy, callback) 
    #Instantiate the node
	rospy.init_node(node_name)
    #Keep running the program until the node is shutdown
	rospy.spin()

#Main of the program
if __name__=='__main__':
	start()
	